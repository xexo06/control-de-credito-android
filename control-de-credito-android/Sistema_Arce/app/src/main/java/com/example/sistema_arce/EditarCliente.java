package com.example.sistema_arce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class EditarCliente extends AppCompatActivity {

    EditText nombre, apellido, detalle, rut, telefono, monto;
    Button bt_editar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_cliente);

        nombre = (EditText) findViewById(R.id.et_fkcliente);
        apellido= (EditText) findViewById(R.id.et_apellido);
        rut= (EditText) findViewById(R.id.et_rut);
        detalle = (EditText) findViewById(R.id.et_detalle);
        telefono = (EditText) findViewById(R.id.et_telefono);
        monto = (EditText) findViewById(R.id.et_monto);
        bt_editar = (Button) findViewById(R.id.bt_editar);


        nombre.setText((String)getIntent().getSerializableExtra("nombre"));
        apellido.setText((String)getIntent().getSerializableExtra("apellido"));
        rut.setText((String)getIntent().getSerializableExtra("rut"));
        detalle.setText((String)getIntent().getSerializableExtra("detalle"));
        telefono.setText((String)getIntent().getSerializableExtra("telefono"));
        monto.setText((String)getIntent().getSerializableExtra("monto"));


        bt_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id= getIntent().getStringExtra("id");

                Toast.makeText(getApplicationContext(),"id="+id,Toast.LENGTH_LONG).show();


                String consulta = "http://pi/Sistema_Arce/Cliente/editarCliente.php?id=" + id + "&nombre=" + nombre.getText() +
                        "&apellido=" +apellido.getText() + "&rut=" + rut.getText() + "&detalle=" +detalle.getText()+ "&telefono=" +telefono.getText() +
                        "&monto=" +monto.getText();
                EditarDatos(consulta);
                onBackPressed();

            }
        });

    }


    public void EditarDatos(String URL) {




        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][", ",");


                Toast.makeText(getApplicationContext(), "funciona" , Toast.LENGTH_LONG).show();


                try {

                    JSONArray ja = new JSONArray(response);

                    Log.i("sizejson", "" + ja.length());



                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no funciono" , Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }
}
