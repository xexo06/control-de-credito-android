package com.example.sistema_arce;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentCredito extends Fragment {

    Button btnCrearCredito, btnBuscarCredito;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_credito,container,false);

        btnCrearCredito = (Button)  vista.findViewById(R.id.crear_credito);
        btnBuscarCredito = (Button)  vista.findViewById(R.id.buscar_credito);

        btnCrearCredito.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v ){
                Intent credito = new Intent(getContext(),CrearCredito.class);
                startActivity(credito);


            }
        });

        btnBuscarCredito.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v ){
                Intent credito = new Intent(getContext(),BuscarCredito.class);
                startActivity(credito);


            }
        });

      return vista;



    }


}
