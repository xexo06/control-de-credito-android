package com.example.sistema_arce;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;


public class BuscarCliente extends AppCompatActivity {

    EditText estado;
    Button   bt_buscar;
    Spinner  atributos;
    String   atributo;
    ListView listaResultado;
    ProgressBar progressBarCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_cliente);

        estado = (EditText) findViewById(R.id.et_buscar);
        atributos = (Spinner) findViewById(R.id.spinner);
        bt_buscar = (Button) findViewById(R.id.bt_buscarCreditos);
        listaResultado = (ListView) findViewById(R.id.lvList2);

        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.atributos_cliente,android.R.layout.simple_spinner_item);

        atributos.setAdapter(adapter);

        atributos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(parent.getContext(),"selecionado" +parent.getItemAtPosition(position).toString(),Toast.LENGTH_LONG).show();
                atributo=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        bt_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              if(!estado.getText().toString().isEmpty()) {



                    String registro = "http://pi/Sistema_Arce/Cliente/cliente_buscar.php?atributo=" + atributo +
                        "&estado=" + estado.getText();

                  BuscarCliente(registro);
                  new BuscarCliente.AsyncTask_load().execute();
                  bt_buscar.setClickable(false);
              }else{
                  Toast.makeText(getApplicationContext(), "Llene el formulario", Toast.LENGTH_SHORT).show();
              }



            }
        });

    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(2);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            bt_buscar.setClickable(true);

            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }


    public void BuscarCliente(String URL) {




        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                response = response.replace("][", ",");

                if (response.length() > 0) {
                    try {
                        Toast.makeText(getApplicationContext(), "funciona", Toast.LENGTH_SHORT).show();
                        JSONArray ja = new JSONArray(response);
                        Log.i("sizejson", "" + ja.length());
                        CargarListView2(ja);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }

    public void CargarListView2(JSONArray ja) {

        //ArrayList<String> lista = new ArrayList<>();

        int tamaño = ja.length()/8;
        final String arreglo[][]= new String[tamaño][9];
        int j = 0;




        for (int i = 0; i < ja.length(); i += 8) {

            try {

                //lista.add(ja.getString(i) + " " + ja.getString(i + 1) + " " + ja.getString(i + 2) + " " + ja.getString(i + 3)+" eliminar"+" editar");

                arreglo[j][0] = ja.getString(i+2);
                arreglo[j][1] = ja.getString(i+3);
                arreglo[j][2] = ja.getString(i+4);
                arreglo[j][3] = ja.getString(i+5);
                arreglo[j][4] = ja.getString(i+6);
                arreglo[j][5] = ja.getString(i+7);
                arreglo[j][6] = "eliminar";
                arreglo[j][7] = "editar";
                arreglo[j][8] = ja.getString(i);
                j++;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }




        listaResultado.setAdapter(new CrearClienteAdaptador(this,arreglo));


    /*  ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        listaResultado.setAdapter(new Adaptador2(this,adaptador)); */


    }



}
