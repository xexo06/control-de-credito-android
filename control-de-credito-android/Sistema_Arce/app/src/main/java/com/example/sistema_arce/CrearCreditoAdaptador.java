package com.example.sistema_arce;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class CrearCreditoAdaptador extends BaseAdapter {

    private static LayoutInflater inflater = null;

    Context contexto;
    String[][] datos;


    public CrearCreditoAdaptador(Context conexto, String[][] datos)
    {
        this.contexto = conexto;
        this.datos = datos;


        inflater = (LayoutInflater)conexto.getSystemService(conexto.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        final View vista = inflater.inflate(R.layout.credito_elemento_lista, null);



        TextView fkcliente = (TextView) vista.findViewById(R.id.tv_nombre);
        TextView fecha = (TextView) vista.findViewById(R.id.tv_apellido);
        TextView detalles = (TextView) vista.findViewById(R.id.tv_detalles);
        TextView numeroBoleto = (TextView) vista.findViewById(R.id.tv_detalle);
        TextView saldo = (TextView) vista.findViewById(R.id.tv_rut);


        Button eliminar = (Button) vista.findViewById(R.id.bt_eliminar);
        Button editar = (Button) vista.findViewById(R.id.bt_editar);


        fkcliente.setText(datos[i][1]);
        numeroBoleto.setText( datos[i][2]);
        detalles.setText( datos[i][3]);
        saldo.setText( datos[i][4]);
        fecha.setText( datos[i][5]);
        eliminar.setText(datos[i][6]);
        editar.setText(datos[i][7]);

        //imagen.setTag(i);

        eliminar.setTag(i);
        editar.setTag(i);


        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent creditoEditar = new Intent(contexto, EditarCredito.class);
                creditoEditar.putExtra("id", datos[(Integer)v.getTag()][0]);
                creditoEditar.putExtra("fkCliente", datos[(Integer)v.getTag()][1]);
                creditoEditar.putExtra("numeroBoleto", datos[(Integer)v.getTag()][2]);
                creditoEditar.putExtra("detalles", datos[(Integer)v.getTag()][3]);
                creditoEditar.putExtra("saldo", datos[(Integer)v.getTag()][4]);

                contexto.startActivity(creditoEditar);
            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eliminar = "http://pi/Sistema_Arce/Credito/eliminarCredito.php?id="+datos[(Integer)v.getTag()][0];
                EliminarDatos(eliminar);
            }
        });


        return vista;
    }

    public void EliminarDatos(String URL) {




        RequestQueue queue = Volley.newRequestQueue(contexto);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][", ",");


                Toast.makeText(contexto, "funciono" , Toast.LENGTH_SHORT).show();


                try {

                    JSONArray ja = new JSONArray(response);
                    Log.i("sizejson", "" + ja.length());

                    Toast.makeText(contexto, "=" + ja.getString(0), Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(contexto, "no funciono" , Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }



    @Override
    public int getCount() {
        return datos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }






}
