package com.example.sistema_arce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class BuscarPago extends AppCompatActivity {

    EditText estado;
    Button bt_buscar;
    Spinner atributos;

    ListView listaResultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_pago);

        estado = (EditText) findViewById(R.id.et_buscar);

        bt_buscar = (Button) findViewById(R.id.bt_buscarCreditos);
        listaResultado = (ListView) findViewById(R.id.lvPago);



        bt_buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!estado.getText().toString().isEmpty()) {
                    String registro = "http://pi/Sistema_Arce/Pago/buscarPago.php?atributo=fk_cliente" + "&estado=" + estado.getText();
                    BuscarPago(registro);
                }else{
                    Toast.makeText(getApplicationContext(),"Llene el formulario",Toast.LENGTH_SHORT).show();

                }


            }
        });

    }

    public void  BuscarPago(String URL) {





        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                response = response.replace("][", ",");

                if (response.length() != 4) {
                    try {
                        JSONArray ja = new JSONArray(response);
                        Log.i("sizejson", "" + ja.length());
                        Toast.makeText(getApplicationContext(), ja.getString(0), Toast.LENGTH_SHORT).show();
                        CargarListView2(ja);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    Toast.makeText(getApplicationContext(), "no hay pago", Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "asdasdas", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }

    public void CargarListView2(JSONArray ja) {

        //ArrayList<String> lista = new ArrayList<>();

        int tamaño = ja.length()/4;
        final String arreglo[][]= new String[tamaño][6];
        int j = 0;




        for (int i = 0; i < ja.length(); i += 4) {

            try {

                //lista.add(ja.getString(i) + " " + ja.getString(i + 1) + " " + ja.getString(i + 2) + " " + ja.getString(i + 3)+" eliminar"+" editar");

                arreglo[j][0] = ja.getString(i);
                arreglo[j][1] = ja.getString(i+1);
                arreglo[j][2] = ja.getString(i+2);
                arreglo[j][3] = ja.getString(i+3);
                arreglo[j][4] = "eliminar";
                arreglo[j][5] = "editar";
                j++;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }




        listaResultado.setAdapter(new CrearPagoAdaptador(this,arreglo));


    /*  ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        listaResultado.setAdapter(new Adaptador2(this,adaptador)); */


    }


}
