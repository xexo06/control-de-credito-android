package com.example.sistema_arce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class EditarPago extends AppCompatActivity {

    EditText et_fkCliente, et_monto;
    Button   bt_editar;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_pago);

        id =(String)getIntent().getSerializableExtra("id");
        et_fkCliente = (EditText) findViewById(R.id.et_fkcliente);
        et_monto = (EditText) findViewById(R.id.et_monto);
        bt_editar = (Button) findViewById(R.id.bt_editar);


        et_fkCliente.setText((String)getIntent().getSerializableExtra("fkCliente"));
        et_monto.setText((String)getIntent().getSerializableExtra("monto"));

       ;


        bt_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String registro ="http://pi/Sistema_Arce/Pago/editarPago.php?id="+id+"&fkCliente="+et_fkCliente.getText()+"&monto="+et_monto.getText();
                EditarDatos(registro);
                onBackPressed();
            }
        });

    }

    private void EditarDatos(String URL) {


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][", ",");


                Toast.makeText(getApplicationContext(), "funciona" , Toast.LENGTH_LONG).show();


                try {

                    JSONArray ja = new JSONArray(response);

                    Log.i("sizejson", "" + ja.length());



                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no funciono" , Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);
    }
}
