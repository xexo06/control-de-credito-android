package com.example.sistema_arce;

public class ObjetCreditoDeuda {

    private String fkCliente, deudaTotal, pago, deudaActual;


    public String getFkCliente() {
        return fkCliente;
    }

    public void setFkCliente(String fkCliente) {
        this.fkCliente = fkCliente;
    }

    public String getDeudaTotal() {
        return deudaTotal;
    }

    public void setDeudaTotal(String deudaTotal) {
        this.deudaTotal = deudaTotal;
    }

    public String getPago() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public String getDeudaActual() {
        return deudaActual;
    }

    public void setDeudaActual(String deudaActual) {
        this.deudaActual = deudaActual;
    }
}
