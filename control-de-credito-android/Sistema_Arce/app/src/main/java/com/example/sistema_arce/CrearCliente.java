package com.example.sistema_arce;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class CrearCliente extends AppCompatActivity {


    EditText et_nombre, et_apellido, et_rut,et_detalles, et_telefono,et_monto;
    Button btnGuardar, btnCargar;
    ListView listaResultado;
    ProgressBar progressBarCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cliente);

        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);

        et_nombre = (EditText) findViewById(R.id.et_fkcliente);
        et_apellido = (EditText) findViewById(R.id.et_apellido);
        et_rut = (EditText) findViewById(R.id.et_rut);
        et_detalles = (EditText) findViewById(R.id.et_detalle);
        et_telefono = (EditText) findViewById(R.id.et_telefono);
        et_monto = (EditText) findViewById(R.id.et_monto);

        btnGuardar = (Button) findViewById(R.id.bt_guardar);
        btnCargar = (Button) findViewById(R.id.bt_cargar);
        listaResultado = (ListView) findViewById(R.id.lvLista);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(!et_nombre.getText().toString().isEmpty() && !et_apellido.getText().toString().isEmpty()
                && !et_rut.getText().toString().isEmpty() && !et_telefono.getText().toString().isEmpty()
                && !et_monto.getText().toString().isEmpty()) {




                   String registro = "http://pi/Sistema_Arce/Cliente/agregarCliente.php?nombre=" + et_nombre.getText() + "&apellido=" + et_apellido.getText() + "&rut=" + et_rut.getText()
                       + "&detalle=" + et_detalles.getText() + "&telefono=" + et_telefono.getText() + "&monto=" + et_monto.getText();

                    EnviarRecibirDatos(registro);
                }else{
                    Toast.makeText(getApplicationContext(),"Llene el formulario",Toast.LENGTH_SHORT).show();
                }
                new CrearCliente.AsyncTask_load().execute();
                btnGuardar.setClickable(false);
                btnCargar.setClickable(false);
            }
        });
        btnCargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //S
                String consulta = "http://pi/Sistema_Arce/Cliente/leerCliente.php";
                EnviarRecibirDatos(consulta);
                new CrearCliente.AsyncTask_load().execute();
                btnGuardar.setClickable(false);
                btnCargar.setClickable(false);


            }
        });
    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            Toast.makeText(CrearCliente.this,"Cagando", Toast.LENGTH_SHORT).show();
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(5);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {

            btnGuardar.setClickable(true);
            btnCargar.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }

        public void EnviarRecibirDatos(String URL) {




            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {



                    response = response.replace("][", ",");

                    if (response.length() !=18 && response.length()!=0) {
                        try {

                            JSONArray ja = new JSONArray(response);
                            Log.i("sizejson", "" + ja.length());
                            CargarListView2(ja);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }else if(response.length() == 0){
                        Toast.makeText(getApplicationContext(),"No hay cliente en la base de datos",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"El rut ya existe",Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
                }
            });

            queue.add(stringRequest);

        }

        public void CargarListView2(JSONArray ja) {

            //ArrayList<String> lista = new ArrayList<>();

            int tamaño = ja.length()/8;
            final String arreglo[][]= new String[tamaño][9];
            int j = 0;




            for (int i = 0; i < ja.length(); i += 8) {

                try {

                    //lista.add(ja.getString(i) + " " + ja.getString(i + 1) + " " + ja.getString(i + 2) + " " + ja.getString(i + 3)+" eliminar"+" editar");

                    arreglo[j][0] = ja.getString(i+2);
                    arreglo[j][1] = ja.getString(i+3);
                    arreglo[j][2] = ja.getString(i+4);
                    arreglo[j][3] = ja.getString(i+5);
                    arreglo[j][4] = ja.getString(i+6);
                    arreglo[j][5] = ja.getString(i+7);
                    arreglo[j][6] = "eliminar";
                    arreglo[j][7] = "editar";
                    arreglo[j][8] = ja.getString(i);
                    j++;

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }




            listaResultado.setAdapter(new CrearClienteAdaptador(this,arreglo));


    /*  ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        listaResultado.setAdapter(new Adaptador2(this,adaptador)); */


        }


    }

