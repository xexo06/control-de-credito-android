package com.example.sistema_arce;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.URL;

public class CrearCredito extends AppCompatActivity {

    EditText fk_cliente, boleta, deuda, detalles;
    Button  bt_cargar, bt_guardar;
    ListView listaResultado;
    ProgressBar progressBarCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_credito);

        fk_cliente = (EditText) findViewById(R.id.et_FK_Cliente);
        boleta = (EditText) findViewById(R.id.et_numeroBoleta);
        deuda = (EditText) findViewById(R.id.et_deuda);
        detalles = (EditText) findViewById(R.id.et_detalles);

        listaResultado = (ListView) findViewById(R.id.lvList2);

        bt_cargar = (Button) findViewById(R.id.bt_cargar);
        bt_guardar = (Button) findViewById(R.id.bt_guardar);

        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);


        bt_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!fk_cliente.getText().toString().isEmpty() && !boleta.getText().toString().isEmpty()
                        && !deuda.getText().toString().isEmpty() && !detalles.getText().toString().isEmpty()) {


                    String registro = "http://pi/Sistema_Arce/Credito/agregarCredito.php?claveCliente=" + fk_cliente.getText() + "&numeroBoleta=" + boleta.getText() + "&deuda=" + deuda.getText() + "&detalles=" + detalles.getText();
                    GuardarDatosCreditos(registro);
                    new CrearCredito.AsyncTask_load().execute();
                    bt_guardar.setClickable(false);
                    bt_cargar.setClickable(false);
                } else {

                   Toast.makeText(getApplicationContext(),"Llene el formulario",Toast.LENGTH_LONG).show();
                }
            }
        });

        bt_cargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String registro = "http://pi/Sistema_Arce/Credito/leerCreditos.php";
                GuardarDatosCreditos(registro);
                new CrearCredito.AsyncTask_load().execute();
                bt_guardar.setClickable(false);
                bt_cargar.setClickable(false);
            }
        });
    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            Toast.makeText(CrearCredito.this,"Cagando", Toast.LENGTH_SHORT).show();
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(2);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {

            bt_guardar.setClickable(true);
            bt_cargar.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }

    public void  GuardarDatosCreditos(String URL) {





        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                response = response.replace("][", ",");


               if(response.length() == 6) {
                   Toast.makeText(getApplicationContext(), "Ya éxiste la boleta", Toast.LENGTH_SHORT).show();

               }if (response.length() == 5) {
                    Toast.makeText(getApplicationContext(), "No éxiste el cliente", Toast.LENGTH_SHORT).show();

                }else if (response.length() == 4) {

                    Toast.makeText(getApplicationContext(), "No hay creditos", Toast.LENGTH_SHORT).show();
                }else if(response.length() == 0) {

                    Toast.makeText(getApplicationContext(), "Se agregó con existo", Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(), "Aprete el boton Cargar", Toast.LENGTH_SHORT).show();

                }else{

                    try {

                        JSONArray ja = new JSONArray(response);
                        Log.i("sizejson", "" + ja.length());
                        CargarListView2(ja);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }

    public void CargarListView2(JSONArray ja) {

        //ArrayList<String> lista = new ArrayList<>();

        int tamaño = ja.length()/6;
        final String arreglo[][]= new String[tamaño][8];
        int j = 0;




        for (int i = 0; i < ja.length(); i += 6) {

            try {

                //lista.add(ja.getString(i) + " " + ja.getString(i + 1) + " " + ja.getString(i + 2) + " " + ja.getString(i + 3)+" eliminar"+" editar");

                arreglo[j][0] = ja.getString(i);
                arreglo[j][1] = ja.getString(i+1);
                arreglo[j][2] = ja.getString(i+2);
                arreglo[j][3] = ja.getString(i+3);
                arreglo[j][4] = ja.getString(i+4);
                arreglo[j][5] = ja.getString(i+5);
                arreglo[j][6] = "eliminar";
                arreglo[j][7] = "editar";
                j++;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }




        listaResultado.setAdapter(new CrearCreditoAdaptador(this,arreglo));


    /*  ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        listaResultado.setAdapter(new Adaptador2(this,adaptador)); */


    }

}
