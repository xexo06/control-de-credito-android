package com.example.sistema_arce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class EditarCredito extends AppCompatActivity {


    EditText fkCliente, boleta, detalles, deuda, telefono;
    Button bt_editar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_credito);


        fkCliente = (EditText) findViewById(R.id.et_FK_Cliente);
        boleta= (EditText) findViewById(R.id.et_numeroBoleta);
        deuda= (EditText) findViewById(R.id.et_deuda);
        detalles = (EditText) findViewById(R.id.et_detalles);
        bt_editar = (Button) findViewById(R.id.bt_editarCredito);



        fkCliente.setText((String)getIntent().getSerializableExtra("fkCliente"));
        boleta.setText((String)getIntent().getSerializableExtra("numeroBoleto"));
        deuda.setText((String)getIntent().getSerializableExtra("saldo"));
        detalles.setText((String)getIntent().getSerializableExtra("detalles"));




          bt_editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id= getIntent().getStringExtra("id");



                String consulta = "http://pi/Sistema_Arce/Credito/editarCredito.php?id=" + id + "&fkCliente=" + fkCliente.getText() +
                        "&numeroBoleto=" +boleta.getText() + "&detalles=" + detalles.getText() + "&deuda=" +deuda.getText();
                EditarDatos(consulta);
                onBackPressed();

            }
        });


    }


    public void  EditarDatos(String URL) {


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][", ",");


                Toast.makeText(getApplicationContext(), "Verifique que de haya Modificado" , Toast.LENGTH_LONG).show();


                try {

                    JSONArray ja = new JSONArray(response);

                    Log.i("sizejson", "" + ja.length());



                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "no funciono" , Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);


    }


}
