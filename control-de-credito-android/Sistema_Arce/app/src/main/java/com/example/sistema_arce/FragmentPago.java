package com.example.sistema_arce;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentPago extends Fragment {


    Button btnCrearPago, btnBuscarPago;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_pago, container, false);

        btnCrearPago = (Button) vista.findViewById(R.id.pago_crear);
        btnBuscarPago = (Button) vista.findViewById(R.id.pago_buscar);



        btnCrearPago.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent pago = new Intent(getContext(), CrearPago.class);
                startActivity(pago);
            }
        });

        btnBuscarPago.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent pago = new Intent(getContext(), BuscarPago.class);
                startActivity(pago);
            }
        });

      return vista;
    }

}

