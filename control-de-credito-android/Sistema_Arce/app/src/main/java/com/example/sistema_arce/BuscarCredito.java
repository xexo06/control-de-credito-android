package com.example.sistema_arce;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BuscarCredito extends AppCompatActivity {


    EditText estado;
    Button bt_buscarCredito , bt_buscarDeuda, bt_buscarBoleta, bt_resumenDeuda;
    String atributo;
    ListView listaResultado;
    ListView listaResultado2;
    LayoutInflater inflater = null;
    ProgressBar progressBarCircular;
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_credito);

        estado = (EditText) findViewById(R.id.et_buscar);
        bt_buscarCredito = (Button) findViewById(R.id.bt_buscarCreditos);
        bt_resumenDeuda = (Button) findViewById(R.id.bt_resumenDeuda);

        bt_buscarBoleta = (Button) findViewById(R.id.bt_buscarBoleta);
        listaResultado = (ListView) findViewById(R.id.lvList3);

        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);

        queue = Volley.newRequestQueue(this);






        bt_buscarCredito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!estado.getText().toString().isEmpty()) {

                    String registro = "http://pi/Sistema_Arce/Credito/buscarCredito.php?atributo=fk_cliente&estado=" + estado.getText();
                    BuscarCredito(registro);


                    new BuscarCredito.AsyncTask_load().execute();
                    bt_buscarCredito.setClickable(false);
                    bt_resumenDeuda.setClickable(false);
                    bt_buscarBoleta.setClickable(false);
                }else{
                    Toast.makeText(getApplicationContext(),"Llene el formulario",Toast.LENGTH_SHORT).show();
                }
            }
        });



        bt_resumenDeuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String registro2 = "http://pi/Sistema_Arce/Credito/calcularCredito.php?estado=" + estado.getText();
                calcularCredito(registro2);

                new BuscarCredito.AsyncTask_load().execute();
                bt_buscarCredito.setClickable(false);
                bt_resumenDeuda.setClickable(false);
                bt_buscarBoleta.setClickable(false);

            }
        });

        bt_buscarBoleta.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                 String registro2 = "http://pi/Sistema_Arce/Credito/buscarBoleta.php?estado=" + estado.getText();
                buscarBoleta(registro2);
                new BuscarCredito.AsyncTask_load().execute();
                bt_buscarCredito.setClickable(false);
                bt_resumenDeuda.setClickable(false);
                bt_buscarBoleta.setClickable(false);
            }
        });
    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(2);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            bt_buscarCredito.setClickable(true);
            bt_resumenDeuda.setClickable(true);
            bt_buscarBoleta.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }




    public void  BuscarCredito(String URL) {





            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {



                    response = response.replace("][", ",");

                    if (response.length() != 3) {
                        Toast.makeText(getApplicationContext(),"Se encontraron los creditos",Toast.LENGTH_SHORT).show();
                        try {
                            JSONArray ja = new JSONArray(response);
                            Log.i("sizejson", "" + ja.length());
                            CargarListView2(ja);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }else{
                        Toast.makeText(getApplicationContext(),"El dato no existe",Toast.LENGTH_SHORT).show();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
                }
            });

            queue.add(stringRequest);

        }

        public void CargarListView2(JSONArray ja) {

            //ArrayList<String> lista = new ArrayList<>();

            int tamaño = ja.length() / 6;
            final String arreglo[][] = new String[tamaño][8];
            int j = 0;


            for (int i = 0; i < ja.length(); i += 6) {

                try {

                    //lista.add(ja.getString(i) + " " + ja.getString(i + 1) + " " + ja.getString(i + 2) + " " + ja.getString(i + 3)+" eliminar"+" editar");

                    arreglo[j][0] = ja.getString(i);
                    arreglo[j][1] = ja.getString(i + 1);
                    arreglo[j][2] = ja.getString(i + 2);
                    arreglo[j][3] = ja.getString(i + 3);
                    arreglo[j][4] = ja.getString(i + 4);
                    arreglo[j][5] = ja.getString(i + 5);
                    arreglo[j][6] = "eliminar";
                    arreglo[j][7] = "editar";
                    j++;

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            listaResultado.setAdapter(new CrearCreditoAdaptador(this, arreglo));


    /*  ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        listaResultado.setAdapter(new Adaptador2(this,adaptador));*/


        }




    public void calcularCredito(String URL) {




      //  Toast.makeText(getApplicationContext(), "funciona" + URL, Toast.LENGTH_LONG).show();



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL,null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {



                ObjetCreditoDeuda deuda = new ObjetCreditoDeuda();
                JSONArray jsonArray = response.optJSONArray("datos");
                JSONObject jsonObject = null;


                try {

                    jsonObject = jsonArray.getJSONObject(0);
                    deuda.setFkCliente(jsonObject.optString("fkCliente"));
                    deuda.setDeudaTotal(jsonObject.optString("deudaTotal"));
                    deuda.setPago(jsonObject.optString("pago"));
                    deuda.setDeudaActual(jsonObject.optString("deudaActual"));
                    String arreglo[][] = new String[1][4];


                    if (deuda.getPago().equalsIgnoreCase("")  ){
                        if(deuda.getDeudaTotal().equalsIgnoreCase("")){
                            arreglo[0][0] = deuda.getFkCliente();
                            arreglo[0][1] = "0";
                            arreglo[0][2] = "0";
                            arreglo[0][3] = "0";
                        }else{
                            if(deuda.getFkCliente().equalsIgnoreCase("null") &&
                                    deuda.getDeudaTotal().equalsIgnoreCase("null")) {
                                Toast.makeText(getApplicationContext(), "El Cliente no tiene deuda", Toast.LENGTH_SHORT).show();
                                arreglo[0][0] = "0";
                                arreglo[0][1] = "0";
                                arreglo[0][2] = "0";
                                arreglo[0][3] = "0";
                            }else {
                                arreglo[0][0] = deuda.getFkCliente();
                                arreglo[0][1] = deuda.getDeudaTotal();
                                arreglo[0][2] = "0";
                                arreglo[0][3] = deuda.getDeudaTotal();
                            }
                        }

                    }else {

                        arreglo[0][0] = deuda.getFkCliente();
                        arreglo[0][1] = deuda.getDeudaTotal();
                        arreglo[0][2] = deuda.getPago();
                        arreglo[0][3] = deuda.getDeudaActual();
                    }

                    listaResultado.setAdapter(new CrearCreditoResultadoAdaptador(getApplication(),arreglo));






                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        }

        );

        queue.add(jsonObjectRequest);

    }

    private void buscarBoleta(String URL) {


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL,null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {



                ObjectCredito credito = new ObjectCredito();
                JSONArray jsonArray = response.optJSONArray("datos");
                JSONObject jsonObject = null;


                try {

                    jsonObject = jsonArray.getJSONObject(0);
                    credito.setId(jsonObject.optString("id"));
                    credito.setFkCliente(jsonObject.optString("fk_cliente"));
                    credito.setNumeroBoleta(jsonObject.optString("numero_boleto"));
                    credito.setDetalles(jsonObject.optString("detalles"));
                    credito.setSaldoDeudor(jsonObject.optString("saldo_deudor"));
                    credito.setFecha(jsonObject.optString("fecha"));

                    String arreglo[][]= new String[1][8];
                    arreglo[0][0] = credito.getId();
                    arreglo[0][1] = credito.getFkCliente();
                    arreglo[0][2] = credito.getNumeroBoleta();
                    arreglo[0][3] = credito.getDetalles();
                    arreglo[0][4] = credito.getSaldoDeudor();
                    arreglo[0][5] = credito.getFecha();
                    arreglo[0][6] = "Eliminar";
                    arreglo[0][7] = "Editar";


                    listaResultado.setAdapter(new CrearCreditoAdaptador(getApplication(),arreglo));






                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        }

        );

        queue.add(jsonObjectRequest);

    }


}
