package com.example.sistema_arce;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class CrearClienteAdaptador extends BaseAdapter {

    private static LayoutInflater inflater = null;

    Context contexto;
    String[][] datos;


    public CrearClienteAdaptador(Context conexto, String[][] datos)
    {
        this.contexto = conexto;
        this.datos = datos;


        inflater = (LayoutInflater)conexto.getSystemService(conexto.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        final View vista = inflater.inflate(R.layout.cliente2_elemento_lista, null);

        TextView nombre = (TextView) vista.findViewById(R.id.tv_nombre);
        TextView apellido = (TextView) vista.findViewById(R.id.tv_apellido);
        TextView rut = (TextView) vista.findViewById(R.id.tv_rut);
        TextView detalle = (TextView) vista.findViewById(R.id.tv_detalle);
        TextView telefono = (TextView) vista.findViewById(R.id.tv_telefono);
        TextView credito = (TextView) vista.findViewById(R.id.tv_credito);

        TextView id = (TextView) vista.findViewById(R.id.tv_id);

        Button eliminar = (Button) vista.findViewById(R.id.bt_eliminar);
        Button editar = (Button) vista.findViewById(R.id.bt_editar);



        nombre.setText(datos[i][0]);
        apellido.setText(datos[i][1]);
        rut.setText( datos[i][2]);
        detalle.setText( datos[i][3]);
        telefono.setText( datos[i][4]);
        credito.setText( datos[i][5]);
        eliminar.setText(datos[i][6]);
        editar.setText(datos[i][7]);
        id.setText(datos[i][8]);

        //imagen.setTag(i);

        eliminar.setTag(i);
        editar.setTag(i);


        editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent clienteEditar = new Intent(contexto, EditarCliente.class);
                clienteEditar.putExtra("nombre", datos[(Integer)v.getTag()][0]);
                clienteEditar.putExtra("apellido", datos[(Integer)v.getTag()][1]);
                clienteEditar.putExtra("rut", datos[(Integer)v.getTag()][2]);
                clienteEditar.putExtra("detalle", datos[(Integer)v.getTag()][3]);
                clienteEditar.putExtra("telefono", datos[(Integer)v.getTag()][4]);
                clienteEditar.putExtra("monto", datos[(Integer)v.getTag()][5]);
                clienteEditar.putExtra("id",datos[(Integer)v.getTag()][8]);

                contexto.startActivity(clienteEditar);


            }
        });

        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String eliminar = "http://pi/Sistema_Arce/Cliente/eliminarCliente.php?id="+datos[(Integer)v.getTag()][8];
                EliminarDatos(eliminar);


            }
        });


        return vista;
    }

    public void EliminarDatos(String URL) {




        RequestQueue queue = Volley.newRequestQueue(contexto);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                response = response.replace("][", ",");



                Toast.makeText(contexto, "Vuelva a Cargar", Toast.LENGTH_SHORT).show();

                try {

                    JSONArray ja = new JSONArray(response);

                    Log.i("sizejson", "" + ja.length());



                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(contexto, "Hubo un error en el proceso" , Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }



    @Override
    public int getCount() {
        return datos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static void reiniciarActivity(Activity actividad){
        Intent intent=new Intent();
        intent.setClass(actividad, actividad.getClass());
        //llamamos a la actividad
        actividad.startActivity(intent);
        //finalizamos la actividad actual
        actividad.finish();
    }
}
