package com.example.sistema_arce;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CrearCreditoResultadoAdaptador extends BaseAdapter {

    private static LayoutInflater inflater = null;

    Context contexto;
    String[][] datos;

    public CrearCreditoResultadoAdaptador(Context contexto, String[][] datos) {

        this.contexto = contexto;
        this.datos = datos;

        inflater = (LayoutInflater)contexto.getSystemService(contexto.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        final View vista = inflater.inflate(R.layout.credito_calcular_lista, null);

        TextView fkCliente = (TextView) vista.findViewById(R.id.tv_fkCliente);
        TextView deudaTotal = (TextView) vista.findViewById(R.id.tv_deudaTotal);
        TextView pago = (TextView) vista.findViewById(R.id.tv_nombre);
        TextView deudaActual = (TextView) vista.findViewById(R.id.tv_deudaActual);

        fkCliente.setText(datos[0][0]);
        deudaTotal.setText(datos[0][1]);
        pago.setText(datos[0][2]);
        deudaActual.setText(datos[0][3]);

        return vista;


    }


    @Override
    public int getCount() {
        return datos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }




}
