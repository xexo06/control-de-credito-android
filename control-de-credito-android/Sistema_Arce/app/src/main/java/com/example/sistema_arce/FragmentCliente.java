package com.example.sistema_arce;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentCliente extends Fragment {

    Button btnCrearCliente;
    Button btnBuscarCliente;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

       View vista = inflater.inflate(R.layout.fragment_cliente,container,false);

      btnCrearCliente = (Button) vista.findViewById(R.id.crear_cliente);
      btnBuscarCliente = (Button) vista.findViewById(R.id.buscar_cliente);


      btnCrearCliente.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View v ){
              Intent cliente = new Intent(getContext(),CrearCliente.class);
              startActivity(cliente);


          }
      });

        btnBuscarCliente.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v ){
                Intent cliente = new Intent(getContext(),BuscarCliente.class);
                startActivity(cliente);


            }
        });


        return vista;

    }

}
