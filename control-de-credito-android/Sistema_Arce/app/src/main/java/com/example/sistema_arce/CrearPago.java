package com.example.sistema_arce;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

public class CrearPago extends AppCompatActivity {


    EditText monto, fkCliente;
    Button bt_guardar, bt_Cargar;
    ListView listaResultado;
    ProgressBar progressBarCircular;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pago);

        monto = (EditText) findViewById(R.id.et_monto);
        fkCliente = (EditText) findViewById(R.id.et_fkcliente);
        listaResultado = (ListView) findViewById(R.id.lvPago);

        progressBarCircular = (ProgressBar)findViewById(R.id.progressBarCircular);
        progressBarCircular.getIndeterminateDrawable()
                .setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);

        bt_Cargar = (Button) findViewById(R.id.bt_cargar);
        bt_guardar = (Button) findViewById(R.id.bt_guardar);

        bt_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!fkCliente.getText().toString().isEmpty() && !monto.getText().toString().isEmpty()) {
                    String registro = "http://pi/Sistema_Arce/Pago/agregarPago.php?fkCliente=" + fkCliente.getText() + "&monto=" + monto.getText();
                    EnviarRecibirDatos(registro);
                    new CrearPago.AsyncTask_load().execute();
                    bt_guardar.setClickable(false);
                    bt_Cargar.setClickable(false);
                }else{
                    Toast.makeText(getApplicationContext(),"Llene el formulario",Toast.LENGTH_SHORT).show();
                }
            }
        });

        bt_Cargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    String consulta = "http://pi/Sistema_Arce/Pago/leerPago.php";
                    EnviarRecibirDatos(consulta);
                    new CrearPago.AsyncTask_load().execute();
                    bt_guardar.setClickable(false);
                    bt_Cargar.setClickable(false);


            }
        });


    }

    public class AsyncTask_load extends AsyncTask<Void, Integer, Void> {

        int progreso;


        @Override
        protected void onPreExecute() {
            progreso = 0;
            progressBarCircular.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {

            while(progreso < 100){
                progreso++;
                publishProgress(progreso);
                SystemClock.sleep(2);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... values) {


            progressBarCircular.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            bt_guardar.setClickable(true);
            bt_Cargar.setClickable(true);
            progressBarCircular.setVisibility(View.INVISIBLE);

        }


    }

    public void EnviarRecibirDatos(String URL) {




        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                response = response.replace("][", ",");

                if (response.length() !=4) {
                    try {
                        Toast.makeText(getApplicationContext(), "Se realizo con exito ", Toast.LENGTH_SHORT).show();
                        JSONArray ja = new JSONArray(response);
                        Log.i("sizejson", "" + ja.length());
                        CargarListView2(ja);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    Toast.makeText(getApplicationContext(),"No existe el cliente",Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);

    }

    public void CargarListView2(JSONArray ja) {

        //ArrayList<String> lista = new ArrayList<>();

        int tamaño = ja.length()/4;
        final String arreglo[][]= new String[tamaño][6];
        int j = 0;




        for (int i = 0; i < ja.length(); i += 4) {

            try {

                //lista.add(ja.getString(i) + " " + ja.getString(i + 1) + " " + ja.getString(i + 2) + " " + ja.getString(i + 3)+" eliminar"+" editar");

                arreglo[j][0] = ja.getString(i);
                arreglo[j][1] = ja.getString(i+1);
                arreglo[j][2] = ja.getString(i+2);
                arreglo[j][3] = ja.getString(i+3);
                arreglo[j][4] = "eliminar";
                arreglo[j][5] = "editar";
                j++;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }




        listaResultado.setAdapter(new CrearPagoAdaptador(this,arreglo));


    /*  ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        listaResultado.setAdapter(new Adaptador2(this,adaptador)); */


    }

}
